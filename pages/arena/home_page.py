from selenium.webdriver.common.by import By


class HomePage:

    #Inicjalizacja klasy - przekazanie drivera/przeglądarki
    def __init__(self, browser):
        self.browser = browser

        # otwarcie
    def click_logout(self):
        self.browser.find_element(By.CSS_SELECTOR, '.icons-switch').click()

    def click_on_administrator_link(self):
        self.browser.find_element(By.CSS_SELECTOR, '.header_admin a').click()

        ###