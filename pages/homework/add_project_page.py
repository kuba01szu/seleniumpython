from selenium.webdriver.common.by import By


class AddProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def check_is_name_line_displayed(self):
        self.browser.find_element(By.CSS_SELECTOR, '#name')
        assert self.browser.find_element(By.CSS_SELECTOR, 'div.formContainer.full').is_displayed()

    def create_project_name_prefix_and_description(self, project_name, prefix, texts):
        self.browser.find_element(By.CSS_SELECTOR, '#name').send_keys(project_name)
        self.browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(prefix)
        self.browser.find_element(By.CSS_SELECTOR, '#description').send_keys(texts)

    def verify_project_data(self, project_name, prefix):
        assert len(project_name) == 10
        assert len(prefix) <= 6
        assert project_name.isupper()
        assert prefix.isupper()

    def click_to_save_project(self):
        self.browser.find_element(By.CSS_SELECTOR, 'span#save').click()












