from selenium.webdriver.common.by import By


class DataPage:
    def __init__(self, browser):
        self.browser = browser

    def click_to_close_project_message(self):
        self.browser.find_element(By.CSS_SELECTOR, '.j_close_button').click()

    def verify_generated_name(self, project_name):
        project_generated_name = self.browser.find_element(By.CSS_SELECTOR, '.content_label_title').text
        assert project_generated_name == project_name

    def click_to_find_project_page(self):
        self.browser.find_element(By.CSS_SELECTOR, '.activeMenu').click()




