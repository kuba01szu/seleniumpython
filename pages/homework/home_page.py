from selenium.webdriver.common.by import By


class HomePage:
    def __init__(self, browser):
        self.browser = browser

    def verify_login_user(self, administrator_email):
        user_email = self.browser.find_element(By.CSS_SELECTOR, '.user-info small').text
        assert user_email == administrator_email

    def click_on_administrator_button(self):
        self.browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()


