
from selenium.webdriver.common.by import By


class ProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def verify_elements_on_project_page(self):
        found_projects = self.browser.find_elements(By.CSS_SELECTOR, 'tbody tr')
        assert len(found_projects) > 0
        assert self.browser.find_element(By.CSS_SELECTOR, '#search').is_displayed()

    def click_on_add_new_project_button(self):
        self.browser.find_element(By.LINK_TEXT, 'DODAJ PROJEKT').click()

    def verify_project_found(self, prefix, project_name):
        founded_project_prefix = self.browser.find_element(By.CSS_SELECTOR, '.t_number').text
        assert prefix == founded_project_prefix
        found_projects = self.browser.find_elements(By.CSS_SELECTOR, 'tbody tr')
        assert project_name in found_projects[0].text

    def search_for_project(self, project_name):
        self.browser.find_element(By.CSS_SELECTOR, '#search').send_keys(project_name)
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()






