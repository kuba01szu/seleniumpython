import re
import time

import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture
def browser():
    #część która wykona się przed każdym testem
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    browser.get('https://awesome-testing.blogspot.com')
    #coś co przekazujemy do każdego testu
    yield browser
    #część która wykona się po każdym testem
    browser.quit()

def test_post_count(browser):



    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    # Asercja że lista ma 4 elementy
    assert len(posts) == 4



def test_post_count_after_search(browser):


    # Inicjalizacja searchbara i przycisku search
    browser.find_element(By.CSS_SELECTOR, '.gsc-input input').send_keys('selenium')
    browser.find_element(By.CSS_SELECTOR, '.gsc-search-button input').click()
    titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    assert len(titles) == 20



def test_post_count_on_cypress_label(browser):
    years =browser.find_elements(By.CSS_SELECTOR, '#BlogArchive1_ArchiveList > ul')
    expected_number_of_posts = extract_the_number_of_posts_from_text(years[3].text)

    # Kliknięcie na labelkę
    browser.find_element(By.LINK_TEXT, '2019').click()

    # Czekanie na stronę
    time.sleep(3)
    # Pobranie listy tytułów
    titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    # Asercja że lista ma tyle elementów ile wyświetla się na UI
    assert len(titles) == expected_number_of_posts


def extract_the_number_of_posts_from_text(text):
    match = re.search(r'\((\d+)\)', text)
    return int(match.group(1))
