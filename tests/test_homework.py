from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import string
import random
import lorem


def get_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))


def test_login():
    administrator_email = 'administrator@testarena.pl'
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    browser.get('http://demo.testarena.pl/zaloguj')

    browser.find_element(By.CSS_SELECTOR, '#email').send_keys('administrator@testarena.pl')
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys('sumXQQ72$L')
    browser.find_element(By.CSS_SELECTOR, '#login').click()

    user_email = browser.find_element(By.CSS_SELECTOR, '.user-info small').text
    assert user_email == administrator_email

    browser.quit()


def test_open_admin_panel():
    administrator_email = 'administrator@testarena.pl'
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    browser.get('http://demo.testarena.pl/zaloguj')

    browser.find_element(By.CSS_SELECTOR, '#email').send_keys(administrator_email)
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys('sumXQQ72$L')
    browser.find_element(By.CSS_SELECTOR, '#login').click()

    browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()

    find_project = browser.find_element(By.CSS_SELECTOR, '#search').text
    assert find_project in browser.current_url

    browser.quit()


def test_add_new_project():
    administrator_email = 'administrator@testarena.pl'
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    browser.get('http://demo.testarena.pl/zaloguj')

    browser.find_element(By.CSS_SELECTOR, '#email').send_keys(administrator_email)
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys('sumXQQ72$L')
    browser.find_element(By.CSS_SELECTOR, '#login').click()

    browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()

    browser.find_element(By.LINK_TEXT, 'DODAJ PROJEKT').click()

    insert_name_line = browser.find_element(By.CSS_SELECTOR, '#name').text
    square_with_color = browser.find_element(By.CSS_SELECTOR, '#inProgressStatusColor').text
    assert insert_name_line in browser.current_url
    assert square_with_color in browser.current_url

    project_name = get_random_string(10)
    prefix = get_random_string(6)
    text = lorem.paragraph()
    
    browser.find_element(By.CSS_SELECTOR, '#name').send_keys(project_name)
    browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(prefix)
    browser.find_element(By.CSS_SELECTOR, '#description').send_keys(text)
    browser.find_element(By.CSS_SELECTOR, 'span#save').click()
    assert len(project_name) == 10
    assert len(prefix) <= 6
    assert project_name.isupper()
    assert prefix.isupper()

    browser.quit()


def test_search_for_project():
    administrator_email = 'administrator@testarena.pl'
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    browser.get('http://demo.testarena.pl/zaloguj')

    browser.find_element(By.CSS_SELECTOR, '#email').send_keys(administrator_email)
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys('sumXQQ72$L')
    browser.find_element(By.CSS_SELECTOR, '#login').click()

    browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()

    browser.find_element(By.LINK_TEXT, 'DODAJ PROJEKT').click()

    insert_name_line = browser.find_element(By.CSS_SELECTOR, '#name').text
    square_with_color = browser.find_element(By.CSS_SELECTOR, '#inProgressStatusColor').text
    assert insert_name_line in browser.current_url
    assert square_with_color in browser.current_url

    project_name = get_random_string(10)
    prefix = get_random_string(6)
    text = lorem.paragraph()

    browser.find_element(By.CSS_SELECTOR, '#name').send_keys(project_name)
    browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(prefix)
    browser.find_element(By.CSS_SELECTOR, '#description').send_keys(text)
    browser.find_element(By.CSS_SELECTOR, 'span#save').click()
    assert len(project_name) == 10
    assert len(prefix) <= 6
    assert project_name.isupper()
    assert prefix.isupper()

    browser.find_element(By.CSS_SELECTOR, '.activeMenu').click()
    browser.find_element(By.CSS_SELECTOR, '#search').send_keys(project_name)
    browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    found_project = browser.find_element(By.CSS_SELECTOR, '.t_number').text
    assert found_project == prefix
    found_projects = browser.find_elements(By.CSS_SELECTOR, 'tbody tr')
    assert project_name in found_projects[0].text

    browser.quit()



