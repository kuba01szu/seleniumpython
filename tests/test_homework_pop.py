import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
import string
import random
import lorem
from pages.homework.add_project_page import AddProjectPage
from pages.homework.data_page import DataPage
from pages.homework.home_page import HomePage
from pages.homework.login_page import LoginPage
from pages.homework.project_page import ProjectPage


def get_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))


administrator_email = 'administrator@testarena.pl'

@pytest.fixture
def browser():

    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    login_page = LoginPage(browser)
    login_page.visit()
    login_page.login(administrator_email, 'sumXQQ72$L')

    yield browser
    browser.quit()


def test_login(browser):
    home_page = HomePage(browser)
    home_page.verify_login_user(administrator_email)


def test_open_admin_panel(browser):
    home_page = HomePage(browser)
    home_page.click_on_administrator_button()

    project_page = ProjectPage(browser)
    project_page.verify_elements_on_project_page()


def test_add_new_project(browser):
    project_name = get_random_string(10)
    prefix = get_random_string(6)
    texts = lorem.paragraph()

    home_page = HomePage(browser)
    home_page.click_on_administrator_button()

    project_page = ProjectPage(browser)
    project_page.click_on_add_new_project_button()

    add_project_page = AddProjectPage(browser)
    add_project_page.check_is_name_line_displayed()
    add_project_page.create_project_name_prefix_and_description(project_name, prefix, texts)
    add_project_page.verify_project_data(project_name, prefix)
    add_project_page.click_to_save_project()


def test_search_for_project(browser):
    project_name = get_random_string(10)
    prefix = get_random_string(6)
    texts = lorem.paragraph()

    home_page = HomePage(browser)
    home_page.click_on_administrator_button()

    project_page = ProjectPage(browser)
    project_page.click_on_add_new_project_button()

    add_project_page = AddProjectPage(browser)
    add_project_page.check_is_name_line_displayed()
    add_project_page.create_project_name_prefix_and_description(project_name, prefix, texts)
    add_project_page.verify_project_data(project_name, prefix)
    add_project_page.click_to_save_project()

    data_page = DataPage(browser)
    data_page.click_to_close_project_message()
    data_page.verify_generated_name(project_name)
    data_page.click_to_find_project_page()

    project_page.search_for_project(project_name)
    project_page.verify_project_found(prefix, project_name)











